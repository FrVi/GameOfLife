#ifndef HEADER

#define HEADER

#include "plan.h"

//void Edit(unsigned char* map, unsigned int x, unsigned int y, unsigned int val=0);
//unsigned int Read(unsigned char* map, unsigned int x, unsigned int y);

void boucle(unsigned char *map,unsigned  char *map2,unsigned int xmin,unsigned int xmax, unsigned int ymin, unsigned int ymax,unsigned char *plan,unsigned char *Plan,unsigned char *Plan2);
unsigned char  Left_Right(unsigned char *map, unsigned char *map2,unsigned int xmin,unsigned int xmax, unsigned int y) ;  
unsigned char  Up_Down(unsigned char *map, unsigned char *map2,unsigned int ymin,unsigned int ymax, unsigned int x) ;
void fun2(int x, int y, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void fun3(int xmin, int ymin, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void fun(int x, int y, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Funct(unsigned char* map,unsigned int x,unsigned int y,unsigned int val_case,unsigned int change,char &val);
void Funct(unsigned char* map,unsigned int x,unsigned int y,unsigned int val_case,unsigned int change);
void Game_Of_Life_v0(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Game_Of_Life_v0_5(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Game_Of_Life_v1(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Game_Of_Life_v2(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Game_Of_Life_v3(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void Game_Of_Life_v4(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
void boucle2(unsigned short *index, unsigned char *map, unsigned char *map2,unsigned int xmin,unsigned int xmax, unsigned int ymin, unsigned int ymax,unsigned char *plan,unsigned char *Plan,unsigned char *Plan2);
void init_index(unsigned short index[65535],unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode);
bool load_index(unsigned short index[65535]);

#endif
