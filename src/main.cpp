#include "menu.h"
//#include "structures.h"
#include <iostream>
#include <string>
//note: il y a un decalgage d'une frame lors du premier blit d'un objet en mode pause
using namespace std;

void Zoom(unsigned char *map,float &val,float fact, int &X, int &Y, float &pos_x, float &pos_y);
void Update(int* temps_P,int mode);

void Zoom(unsigned char *map,float &val,float fact, int &X, int &Y, float &pos_x, float &pos_y)
{
	float X_temp=X;
	float Y_temp=Y;
	float pos_x_temp=pos_x;
	float pos_y_temp=pos_y;
	float val_temp=val;

	if(X_temp>=0 && Y_temp>=0 && X_temp<screensize && Y_temp<screensize)//ne zoom pas si pointeur en dehors de la zone affich�e
	{
	if((X_temp+(texture_menu+hauteur_barre))/fact/val_temp<=taille && (Y_temp+hauteur_barre)/fact/val_temp<=taille)//pas de zoom out si on depasse la taille de la grille
	{
	//on peut quand m�me �tre en dehors de l'ecran
	if( (val_temp<=8 && fact == 2) ||(val_temp>=0.25 && fact == 0.5) )//limites de zoom, arbitraires
	{
		//(X,Y) pointeur de la souris dans l'ecran
		//ce point doit devenir le centre de l'ecran virtuel

		//il faut trouver les coord du nouveau centre de l'ecran physique (avec le menu)
		X_temp=(X_temp+(texture_menu+hauteur_barre)/(2*fact))/val_temp+pos_x_temp;
		Y_temp=(Y_temp+hauteur_barre/(2*fact))/val_temp+pos_y_temp;


		val_temp*=fact;
		glMatrixMode (GL_PROJECTION);
		glLoadIdentity ();

		//largeur : (screensize+texture_menu+barre)/val
		//hauteur : - (screensize+barre)/val

		pos_x_temp=X_temp-(screensize+texture_menu+hauteur_barre)/(2*val_temp);
		pos_y_temp=Y_temp-(screensize+hauteur_barre)/(2*val_temp);
		
		if(pos_x_temp<0)
		{
			pos_x_temp=0;
			X_temp=(screensize+texture_menu+hauteur_barre)/(2*val_temp);
		}

		if(pos_y_temp<0)
		{
			pos_y_temp=0.0;
			Y_temp=(screensize+hauteur_barre)/(2*val_temp);
		}

		if(pos_x_temp+screensize/val_temp>taille)
		{
			pos_x_temp=taille-screensize/val_temp;
			X_temp=pos_x_temp+(screensize+texture_menu+hauteur_barre)/(2*val_temp);
		}

		if(pos_y_temp+screensize/val_temp>taille)
		{
			pos_y_temp=taille-screensize/val_temp;
			Y_temp=pos_y_temp+(screensize+hauteur_barre)/(2*val_temp);
		}

			gluOrtho2D(pos_x_temp, X_temp+(screensize+texture_menu+hauteur_barre)/(2*val_temp), Y_temp+(screensize+hauteur_barre)/(2*val_temp),pos_y_temp); //pour le zoom, mais on doit redessiner alors
			glPointSize(val_temp);
			glMatrixMode (GL_MODELVIEW);
			glLoadIdentity();

			/*X=X_temp;
			Y=Y_temp;*/
			pos_x=pos_x_temp;
			pos_y=pos_y_temp;
			val=val_temp;
		//(pos_x,pos_y) coin en haut � gauche (apr�s le zoom)

		/*if(pos_x<0 || pos_y<0)
		{
			break;
		}*/
		Afficher_Grille(map, pos_x, pos_y,val);
	}
	}
	}
}

void Update(int* temps_P,int mode)
{
	//attention :glCopyPixels (0,0,screensize,screensize
	//(0,0) coin en BAS � gauche...

	int temps_A = SDL_GetTicks();

	if(mode==1)
	{
		if(temps_A - *temps_P >=14)
		{
			*temps_P=temps_A;
			SDL_GL_SwapBuffers();
			glCopyPixels (0,0,screensize,screensize+hauteur_barre,GL_COLOR);
		}
	}
	else if(mode==2)
	{
		if(temps_A - *temps_P >=25)
		{
			*temps_P=temps_A;
			SDL_GL_SwapBuffers();
			glCopyPixels (0,0,screensize,screensize+hauteur_barre,GL_COLOR);
		}
	}
	else 
	{
        SDL_GL_SwapBuffers();
		glCopyPixels (0,0,screensize,screensize+hauteur_barre,GL_COLOR);
		if( temps_A - *temps_P <=14)
		{	 
			SDL_Delay(14-(temps_A - *temps_P));
			*temps_P=temps_A;
		}
	}
}

//la stack est � 10MB au lieu de 1MB std
int main(int argc, char *argv[]) 
{ 
	int mode=1;

	int temps_P        = SDL_GetTicks() ;
	int tempsPrecedent = SDL_GetTicks() ;
	int t_Precedent    = SDL_GetTicks() ;
	int t_Precedent2    = SDL_GetTicks() ;
	int t_Prec   = SDL_GetTicks() ;
	int t_barre   = SDL_GetTicks() ;


	unsigned int x,y,x2,y2;

	int tt = 0;
	int X,Y;

	unsigned char ma[taille*taille8] = {0};
	unsigned char ma2[taille*taille8] = {0};

	unsigned char plan[taille8*taille8] = {0};
	unsigned char Plan[taille8*taille8] = {0};

	unsigned char plan2[taille64*taille64] = {0};
	unsigned char Plan2[taille64*taille64] = {0};

	unsigned char*  map = ma;
	unsigned char* map2 = ma2;
	unsigned char* tr;

	unsigned char* p = plan;
	unsigned char* q = Plan;
	unsigned char* r = plan2;
	unsigned char* s = Plan2;

	float pos_x=0;
	float pos_y=0;

	bool continuer = false;
	bool cond = true;

	float val=1;

	FILE* fichier = NULL;
	fopen_s(&fichier,"Output.txt", "w");

	int indice  = 1;
	int indice2 = 1;
	int indice3 = 0;

	unsigned char ACORN[7*3] = {0};
	unsigned char GLIDER[3*3] = {0};
	unsigned char L_SPACESHIP[5*4] = {0};
	unsigned char GOSPER_GLIDER_GUN[36*9] = {0};
	unsigned char F_PENTAMINO[3*3] = {0};
	unsigned char EXPLODER[5*5] = {0};
	unsigned char UNKNOWN[3*3] = {0};
	unsigned char VOID2[150*150] = {0};
	unsigned char BREEDER_AR[100*128] = {0};
	unsigned char BREEDER_AV[102*128] = {0};
	unsigned char L_GUN[65*44] = {0};
	unsigned char PUFFER[103*33] = {0};

	unsigned char tableau[15*15]= {0};//zone d'edition en bas � droite

	schema *SCHEMA[nbre_schema];

	bool DEFINE=true;

	SCHEMA[0]= new schema(ACORN,7,3,"data/Acorn.data",DEFINE);
	SCHEMA[1]= new schema(GLIDER,3,3,"data/Glider.data",DEFINE);
	SCHEMA[2]= new schema(L_SPACESHIP,5,4,"data/L_spaceship.data",DEFINE);
	SCHEMA[3]= new schema(GOSPER_GLIDER_GUN,36,9,"data/Gosper_glider_gun.data",DEFINE);
	SCHEMA[4]= new schema( F_PENTAMINO,3,3,"data/F_pentamino.data",DEFINE);
	SCHEMA[5]= new schema( EXPLODER, 5,5,"data/Exploder.data",DEFINE);
	SCHEMA[6]= new schema( UNKNOWN,3,3,"data/Unknown.data",DEFINE);
	SCHEMA[7]= new schema(BREEDER_AV,102,128,"data/breederAV.data",DEFINE);
	SCHEMA[8]= new schema(VOID2,150,150,NULL,DEFINE);
	SCHEMA[9]= new schema(BREEDER_AR,100,128,"data/breederAR.data",DEFINE);
	SCHEMA[10]= new schema(L_GUN,65,44,"data/L_gun.data",DEFINE);
	SCHEMA[11]= new schema(PUFFER,103,33,"data/Puffer.data",DEFINE);
	SCHEMA[12]= new schema(tableau,15,15,NULL,DEFINE);

	_putenv("SDL_VIDEO_WINDOW_POS=center");
	SDL_Event event;
	SDL_Surface *ecran = NULL;   

	SDL_Init(SDL_INIT_VIDEO ); 
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);

	//SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	ecran = SDL_SetVideoMode(screensize+texture_menu+hauteur_barre, screensize+hauteur_barre, 32, SDL_OPENGL);

	Projection(val, 0.0f, 0.0f);

	//glTranslatef(0.325, 0.325, 0);
	//glScalef( 1, -1, 1 ) ;

	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glDisable(GL_STENCIL_TEST);

	glReadBuffer (GL_FRONT); 
	glDrawBuffer (GL_BACK); 

	glEnable(GL_TEXTURE_2D);

	bouton *boutons[nbre_boutons];
	boutons[0]= new bouton(x_1,x_1+largeur_bouton, y_1,y_1+hauteur_bouton,0,"data/pause.bmp");//pause
	boutons[1]= new bouton(x_2,x_2+largeur_bouton, y_2,y_2+hauteur_bouton,1,"data/slow.bmp");//slow
	boutons[2]= new bouton(x_3,x_3+largeur_bouton, y_3,y_3+hauteur_bouton,2,"data/normal.bmp");//normal
	boutons[3]= new bouton(x_4,x_4+largeur_bouton, y_4,y_4+hauteur_bouton,3,"data/fast.bmp");//fast
	boutons[4]= new bouton(x_5,x_5+largeur_bouton, y_5,y_5+hauteur_bouton,4,"data/mirror.bmp");//miroir
	boutons[5]= new bouton(x_6,x_6+largeur_bouton, y_6,y_6+hauteur_bouton,5,"data/turn.bmp");//turn
	boutons[6]= new bouton(x_7,x_7+largeur_bouton3, y_7,y_7+hauteur_bouton3,6,"data/breeder_front.bmp");//breeder_av
	boutons[7]= new bouton(x_8,x_8+largeur_bouton, y_8,y_8+hauteur_bouton2,7,"data/glider.bmp");//glider
	boutons[8]= new bouton(x_9,x_9+largeur_bouton, y_9,y_9+hauteur_bouton2,8,"data/gun.bmp");//gg gun
	boutons[9]= new bouton(x_10,x_10+largeur_bouton, y_10,y_10+hauteur_bouton2,9,"data/ship.bmp");//ship
	boutons[10]= new bouton(x_11,x_11+largeur_bouton,y_11,y_11+hauteur_bouton2,10,"data/exploder.bmp");//exploder
	boutons[11]= new bouton(x_12,x_12+largeur_bouton,y_12,y_12+hauteur_bouton2,11,"data/unknown.bmp");//unknown
	boutons[12]= new bouton(x_13,x_13+largeur_bouton,y_13,y_13+hauteur_bouton2,12,"data/f_pentamino.bmp");//f-pentamino
	boutons[13]= new bouton(x_14,x_14+largeur_bouton,y_14,y_14+hauteur_bouton2,13,"data/acorn.bmp");//acorn
	boutons[14]= new bouton(x_15,x_15+largeur_bouton,y_15,y_15+hauteur_bouton2,14,"data/void.bmp");//void
	boutons[15]= new bouton(x_16,x_16+largeur_bouton,y_16,y_16+hauteur_bouton2,15,"data/sauvegarde.bmp");//save
	boutons[16]= new bouton(x_17,x_17+largeur_bouton,y_17,y_17+hauteur_bouton2,16,"data/charger.bmp");//load
	boutons[17]= new bouton(x_18,x_18+largeur_bouton3,y_18,y_18+hauteur_bouton3,17,"data/breeder_back.bmp");//breeder_ar
	boutons[18]= new bouton(x_19,x_19+largeur_bouton,y_19,y_19+hauteur_bouton2,18,"data/vider.bmp");//vider
	boutons[19]= new bouton(x_20,x_20+largeur_bouton,y_20,y_20+hauteur_bouton2,19,"data/blit.bmp");//blit
	boutons[20]= new bouton(x_21,x_21+largeur_bouton3,y_21,y_21+hauteur_bouton3,20,"data/L_gun.bmp");//l_gun
	boutons[21]= new bouton(x_22,x_22+largeur_bouton3,y_22,y_22+hauteur_bouton3,21,"data/Puffer.bmp");//puffer

	menu Menu("data/GUI.bmp",screensize+hauteur_barre,0,screensize+texture_menu+hauteur_barre,screensize+hauteur_barre);

	GLuint texture2;
	texture2= loadTexture("data/barre.bmp",false);
	GLuint texture3;
	texture3= loadTexture("data/onglet3.bmp",false);
	GLuint texture4;
	texture4= loadTexture("data/onglet2.bmp",false);
	GLuint texture5;
	texture5= loadTexture("data/onglet1.bmp",false);

	glDisable(GL_TEXTURE_2D);

	SDL_WM_SetCaption("Jeu de la vie (v3.00) -  Frederic Vitzikam", NULL);   

	SDL_EnableKeyRepeat(1000, 100);

	//SDL_BlitSurface(GUI, NULL,  &positionGUI);

	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapBuffers();
	glClear(GL_COLOR_BUFFER_BIT);

	SCHEMA[0]->Create_Struct(map,700,700,plan,plan2,indice2);
	SCHEMA[1]->Create_Struct(map,400,250,plan,plan2,indice2);
	SCHEMA[2]->Create_Struct(map,250,20,plan,plan2,indice2);
	SCHEMA[3]->Create_Struct(map,80,400,plan,plan2,indice2);
	SCHEMA[4]->Create_Struct(map,200,300,plan,plan2,indice2);
	SCHEMA[5]->Create_Struct(map,50,50,plan,plan2,indice2);
	SCHEMA[6]->Create_Struct(map,50,300,plan,plan2,indice2);
	SCHEMA[7]->Create_Struct(map,500,20,plan,plan2,indice2);
	/*setPixel(0,0,255);
	setPixel(1023,1023,255);*/

	barres Barres(texture2,texture3,texture4,texture5,screensize,screensize+hauteur_barre,0,0,screensize+hauteur_barre,screensize);

	Menu.afficher(val, pos_x, pos_y, indice2, tableau);
	Barres.afficher(val, pos_x, pos_y);

	for(int i=0;i<nbre_boutons;i++)
	{
		boutons[i]->affichage(val,pos_x, pos_y);
	}

	//Afficher_Grille(map);

	SDL_GL_SwapBuffers();

	Menu.affichage_primaire();

	glCopyPixels (0,0,screensize+texture_menu+hauteur_barre,screensize+hauteur_barre,GL_COLOR);

	Constr_de_plan2(map,plan,taille);
	Constr_de_plan2(map,Plan,taille);
	Constr_de_plan(plan,plan2,taille8);

	unsigned short index[65536];

	bool move_cond=false;
	bool cond_barre_horiz=false;
	bool cond_barre_verti=false;
	float fact;
	unsigned int x_min, x_max, y_min, y_max;
	int a,b,i;

	if(!load_index(index))
	{
		init_index(index, &p, &q, &r, &s, &map, &map2,&temps_P, mode);
	}

	if(DEFINE)
	{
		SDL_PollEvent(&event);
		//glClear(GL_COLOR_BUFFER_BIT);

		while(event.type != SDL_QUIT  && tt<50000000 && cond) 
		{   
			SDL_PollEvent(&event);    
			

			if(SDL_GetTicks()-t_Precedent>200)
			{
				if(indice3==1)
				{
					indice3=0;
					t_Precedent=SDL_GetTicks();
					Save(map);
				}
				else  if(indice3==2)
				{
					indice3=0;
					t_Precedent=SDL_GetTicks();
					Load(map,map2,p,q,r,s,pos_x, pos_y,val,indice2, Menu,Barres, tableau);
				}
				else if(indice3==3)
				{
					indice3=0;
					t_Precedent=SDL_GetTicks();
					reset(tableau);
				}
			}

			if( event.type == SDL_MOUSEBUTTONUP || (event.type == SDL_ACTIVEEVENT && event.active.gain == 0) )//perte de focus
			{
				move_cond = false;
				cond_barre_horiz = false;
				cond_barre_verti = false;
			}

			else if(event.type == SDL_MOUSEBUTTONDOWN) 
			{
			
				if ( event.button.button == SDL_BUTTON_LEFT)  
				{ 
					SDL_GetMouseState(&X,&Y);
					if(X>=0 && Y>=0 && X<screensize && Y<screensize && SDL_GetTicks()-t_Precedent>200)//ne cr�e rien en dehors de la zone affich�e
					{
						t_Precedent=SDL_GetTicks();
						X/=val;
						Y/=val;
						X+=pos_x;
						Y+=pos_y;
						if(X>2 && Y>2 && X<taille-SCHEMA[indice]->get_x()-2 && Y<taille-SCHEMA[indice]->get_y()-2)//ne cr�e rien en dehors de la map
						{
							SCHEMA[indice]->Create_Struct(map,X,Y,p,r,indice2);						
						}
					}
					else if(Y<screensize+hauteur_barre-150 && X>screensize+hauteur_barre && SDL_GetTicks()-t_Precedent>200)
					{
						t_Precedent=SDL_GetTicks();
						Fonctions_Menu(boutons,X,Y,indice,indice2, mode,continuer,pos_x,pos_y,val,indice3);
						Afficher_Grille(map, pos_x, pos_y,val);//NECESSAIRE OU PAS?
					}
					else if(X>screensize+hauteur_barre && Y>screensize+hauteur_barre-150 && SDL_GetTicks()-t_Precedent>200)
					{
						t_Precedent=SDL_GetTicks();

						changer_etat(tableau, (X-(screensize+hauteur_barre-1))/10, (Y-(screensize+hauteur_barre-150-1))/10);
					}
					else if(!move_cond)
					{
						Barres.fleches(X,Y,move_cond,a,b,x_min,x_max,y_min,y_max,cond_barre_horiz,cond_barre_verti);
					}
					else if(X>=x_min && X<=x_max && Y>=y_min && Y<=y_max && SDL_GetTicks() - t_Prec>tps_mvt)//move_cond==true : il y a un tour de decalage mais pas grave car rapide
					{
						t_Prec=SDL_GetTicks();
						Move(pos_x,pos_y,a,b, val);
						Afficher_Grille(map, pos_x, pos_y,val);
					}
				}
				else if ( event.button.button == SDL_BUTTON_RIGHT && SDL_GetTicks()-t_Precedent>200)  
				{ 
					t_Precedent=SDL_GetTicks();
					indice++;
					if(indice==9){indice=0;}
				}
				else if ( event.button.button == SDL_BUTTON_MIDDLE && SDL_GetTicks()-t_Precedent>200)  
				{ 
					t_Precedent=SDL_GetTicks();
					indice2++;
					if(indice2==9){indice2=1;}
					Orientation( indice2, pos_x, pos_y, val);
				}
			}
			else if(event.type == SDL_KEYDOWN)
			{
				if(event.key.keysym.sym==SDLK_RETURN)
				{
					if(continuer)
					{
						continuer=false;
					}
					else
					{
						continuer=true;
					}
					SDL_Delay(100);
				}
				else if(event.key.keysym.sym==SDLK_SPACE)
				{
					mode++;
					if(mode>=4){mode=1;}
					event.type=SDL_KEYUP;
				}
				else if(SDL_GetTicks() - t_Prec>tps_mvt)
				{
					switch (event.key.keysym.sym)
					{
						case SDLK_UP:		
						{
							t_Prec=SDL_GetTicks();
							Move(pos_x,pos_y,0,-mvt, val);
							Afficher_Grille(map, pos_x, pos_y,val);
							break;
						}
						case SDLK_DOWN:	
						{
							t_Prec=SDL_GetTicks();
							Move(pos_x,pos_y,0,mvt, val);
							Afficher_Grille(map, pos_x, pos_y,val);
							break;
						}
						case SDLK_RIGHT:
						{
							t_Prec=SDL_GetTicks();
							Move(pos_x,pos_y,mvt,0, val);
							Afficher_Grille(map, pos_x, pos_y,val);
							break;
						}
						case SDLK_LEFT:
						{
							t_Prec=SDL_GetTicks();
							Move(pos_x,pos_y,-mvt,0, val);
							Afficher_Grille(map, pos_x, pos_y,val);
							break;
						}
					}
				}
				else if(SDL_GetTicks()-t_Precedent>200)
				{
					if(event.key.keysym.sym==SDLK_s)
					{
						t_Precedent=SDL_GetTicks();
						Save(map);
					}
					else if(event.key.keysym.sym==SDLK_l)
					{
						t_Precedent=SDL_GetTicks();
						Load(map,map2,p,q,r,s,pos_x, pos_y,val,indice2, Menu,Barres, tableau);
					}
				}
			}

			if(event.type == SDL_MOUSEBUTTONDOWN && SDL_GetTicks()-t_Precedent2>500) 
			{
				t_Precedent2=SDL_GetTicks();
				if(event.button.button == SDL_BUTTON_WHEELUP || event.button.button == SDL_BUTTON_WHEELDOWN)
				{

					if(event.button.button == SDL_BUTTON_WHEELUP)		fact = 2;
					else												fact = 0.5;

					SDL_GetMouseState(&X,&Y);
					Zoom(map,val,fact, X, Y,pos_x, pos_y);
				}

			}

			tt++;

			if(cond_barre_horiz &&  SDL_GetTicks() - t_barre > tps_mvt )
			{
				t_barre=SDL_GetTicks();
				SDL_GetMouseState(&X,&Y);

				Deplacer_Barre_horizontale(Barres, Menu,X, val, pos_x, pos_y, indice2, tableau,map);
				Update(&temps_P,mode);//utile?
			}
			else if(cond_barre_verti &&  SDL_GetTicks() - t_barre > tps_mvt)
			{
				t_barre=SDL_GetTicks();
				SDL_GetMouseState(&X,&Y);

				Deplacer_Barre_verticale(Barres, Menu,Y, val, pos_x, pos_y, indice2, tableau,map);
				Update(&temps_P,mode);//utile?
			}



			if(continuer)
			{
				Afficher_Grille(map, pos_x, pos_y,val);
				//SDL_Delay(100);
			}
			else
			{
				Game_Of_Life_v3(index, &p, &q, &r, &s, &map, &map2,&temps_P,mode);
				//SDL_Delay(10);
			}

			Menu.afficher(val, pos_x, pos_y, indice2, tableau);
			Barres.afficher(val, pos_x, pos_y);
			Update(&temps_P,mode);
		}
	}
	else
	{
		fprintf(fichier,"SOME PATERNS ARE UNDEFINED: a .txt is missing");
	}

	fprintf(fichier," %ld \n", SDL_GetTicks() -tempsPrecedent);

	for(i=0;i<nbre_schema;i++)
	{
		delete SCHEMA[i];
	}

	for(i=0;i<nbre_boutons;i++)
	{
		delete boutons[i];
	}

	SDL_Quit(); 
	fclose(fichier);

	return EXIT_SUCCESS; 
} 
