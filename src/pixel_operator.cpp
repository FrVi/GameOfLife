#include "pixel_operator.h"

void FullBox(unsigned int x, unsigned int x2, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue)
{
	glBegin(GL_QUADS);
		glColor3ub(red,green,blue);
		glVertex2f(x, y);
		glVertex2f(x, y2);
		glVertex2f(x2, y2);
		glVertex2f(x2, y);
	glEnd();     
}

void setPixel(unsigned int x, unsigned int y, GLubyte red, GLubyte green, GLubyte blue)
{
	glBegin(GL_POINTS);
		glColor3ub(red,green,blue);
		glVertex2f(x, y);
	glEnd();
}


void ligneV(unsigned int x, unsigned int y,  unsigned int y2, GLubyte red, GLubyte green, GLubyte blue)
{     
	glColor3ub(red,green,blue);
	
	glBegin(GL_POINTS);
	for(unsigned int i=y;i<=y2;i++)   
	{           
		glVertex2f(x,i);               
	} 
	glEnd();	
}

void ligneH(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue)
{
	glColor3ub(red,green,blue);
	
	glBegin(GL_POINTS);
	for(unsigned int i=x;i<=x2;i++)   
	{           
		glVertex2f(i, y);               
	} 
	glEnd();	
}

void ligneD1(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	glColor3ub(red,green,blue);
	
	glBegin(GL_POINTS);
	for(unsigned int i=0;i<l;i++)   
	{           
		glVertex2f(x+i, y+i);               
	} 
	glEnd();	 
}

void ligneD2(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	glColor3ub(red,green,blue);
	
	glBegin(GL_POINTS);
	for(unsigned int i=0;i<l;i++)   
	{           
		glVertex2f(x+i, y-i);               
	} 
	glEnd();	
}

void fleche_D(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneH(x,x2,y, red, green, blue);
	setPixel(x2-1,y-1, red, green, blue);
	setPixel(x2-2,y-2, red, green, blue);
	setPixel(x2-1,y+1, red, green, blue);
	setPixel(x2-2,y+2, red, green, blue);
}

void fleche_G(unsigned int x, unsigned int x2, unsigned int y, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneH(x,x2,y, red, green, blue);
	setPixel(x+1,y-1,red, green, blue);
	setPixel(x+2,y-2,red, green, blue);
	setPixel(x+1,y+1,red, green, blue);
	setPixel(x+2,y+2,red, green, blue);
}

void fleche_H(unsigned int x, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneV(x,y,y2, red, green, blue);
	setPixel(x-1,y+1, red, green, blue);
	setPixel(x-2,y+2, red, green, blue);
	setPixel(x+1,y+1, red, green, blue);
	setPixel(x+2,y+2, red, green, blue);
}

void fleche_B(unsigned int x, unsigned int y, unsigned int y2, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneV(x,y,y2, red, green, blue);
	setPixel(x-1,y2-1, red, green, blue);
	setPixel(x-2,y2-2, red, green, blue);
	setPixel(x+1,y2-1, red, green, blue);
	setPixel(x+2,y2-2, red, green, blue);
}

void fleche_HG(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneD1(x,y,l, red, green, blue);
	setPixel(x,y+1, red, green, blue);
	setPixel(x,y+2, red, green, blue);
	setPixel(x,y+3, red, green, blue);
	setPixel(x+1,y, red, green, blue);
	setPixel(x+2,y, red, green, blue);
	setPixel(x+3,y, red, green, blue);
}

void fleche_BD(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneD1(x,y,l, red, green, blue);
	setPixel(x+l-1,y+l-2, red, green, blue);
	setPixel(x+l-1,y+l-3, red, green, blue);
	setPixel(x+l-1,y+l-4, red, green, blue);
	setPixel(x+l-2,y+l-1, red, green, blue);
	setPixel(x+l-3,y+l-1, red, green, blue);
	setPixel(x+l-4,y+l-1, red, green, blue);
}

void fleche_BG(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneD2(x,y,l, red, green, blue);
	setPixel(x,y-1, red, green, blue);
	setPixel(x,y-2, red, green, blue);
	setPixel(x,y-3, red, green, blue);
	setPixel(x+1,y, red, green, blue);
	setPixel(x+2,y, red, green, blue);
	setPixel(x+3,y, red, green, blue);
}

void fleche_HD(unsigned int x, unsigned int y,unsigned int l, GLubyte red, GLubyte green, GLubyte blue)
{
	ligneD2(x,y,l, red, green, blue);
	setPixel(x+l-2,y-l+1, red, green, blue);
	setPixel(x+l-3,y-l+1, red, green, blue);
	setPixel(x+l-4,y-l+1, red, green, blue);
	setPixel(x+l-1,y-l+2, red, green, blue);
	setPixel(x+l-1,y-l+3, red, green, blue);
	setPixel(x+l-1,y-l+4, red, green, blue);
}

void setPixel(unsigned int x, unsigned int y, Uint32 coul)
{
	glBegin(GL_POINTS);
		glColor3ub(coul,0,0);
		glVertex2f(x, y);
	glEnd();
}

void setPixel10(unsigned int x, unsigned int y, GLubyte red, GLubyte green, GLubyte blue)
{
	glBegin(GL_QUADS);
		glColor3ub(red,green,blue);
		glVertex2f(x, y);
		glVertex2f(x, y+9);
		glVertex2f(x+9, y+9);
		glVertex2f(x+9, y);
	glEnd();     
}
