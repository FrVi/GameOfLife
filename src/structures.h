#ifndef HEADER2
#define HEADER2

#include <stdlib.h> 
#include <stdio.h> 
#include <SDL\SDL.h> 
#include "vie.h"
#include "bin_operator.h"

bool Lecteur_figure(unsigned char* DATA, char* name, unsigned int x_val, unsigned int y_val);

void Type1(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type2(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type3(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type4(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type5(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type6(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type7(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Type8(unsigned char* DATA,unsigned int dim_x,unsigned int dim_y, unsigned char *map,unsigned int x,unsigned int y);
void Orientation(int indice2, float pos_x, float pos_y, float val);

void Random(unsigned char *map, unsigned int xmin,unsigned int xmax,unsigned int ymin,unsigned int ymax);

#endif
