#include "menu.h"

void Fonctions_Menu(bouton *tab[nbre_boutons], unsigned int x, unsigned int y,int &indice,int &indice2, int &mode, bool &continuer, float pos_x, float pos_y, float val,int &indice3)
{
	if(x>=screensize+hauteur_barre)
	{
		for(int i=0;i<nbre_boutons;i++)
		{
			if(tab[i]->appartient(x,y))
			{
				tab[i]->fonction(mode,continuer, indice, indice2, pos_x, pos_y, val,indice3);
				return;
			}
		}
	}
}

menu::menu(char *text, unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{
	xmin=x1;
	xmax=x2;
	ymin=y1;
	ymax=y2;
	this->texture = loadTexture(text,false);
}

void menu::afficher(float &val, float pos_x, float pos_y, int indice, unsigned char *tableau)
{
	Affichage_Droit(texture,val,xmin,ymin,xmax,ymax, pos_x, pos_y);
	Orientation(indice, pos_x, pos_y, val);
	int x, y,x1,y1;

	Projection(1, 0.0f, 0.0f);

	for(x=0;x<15;x++)
	{
		for(y=0;y<15;y++)
		{
			x1=screensize+hauteur_barre+x*10;
			y1=screensize+hauteur_barre-150+y*10;
			if(tableau[x+y*15]==1)
			{
				setPixel10(x1,y1,255,0,0);
			}
			/*else 
			{
				setPixel10(x1,y1,0,0,0);
			}*/
		}
	}

	Projection(val ,0.0f, 0.0f);
}

void reset(unsigned char tab[15*15])
{
	unsigned int x, y;

	for(x=0;x<15;x++)
	{
		for(y=0;y<15;y++)
		{
			tab[x+y*15]=0;
		}
	}
}

void changer_etat(unsigned char tab[15*15], unsigned int x, unsigned int y)
{
/*	unsigned int x1=screensize+hauteur_barre+1+x*10;
	unsigned int y1=screensize+hauteur_barre+1-150+y*10;*/
	if(tab[x+y*15]==0)
	{
		tab[x+y*15]=1;
		//setPixel10(x1,y1,255,0,0);
	}
	else
	{
		tab[x+y*15]=0;
		//setPixel10(x1,y1,0,0,0);
	}
}

void Load(unsigned char* map, unsigned char* map2, unsigned char* p, unsigned char* q, unsigned char* r, unsigned char* s, float pos_x, float pos_y,float val,int indice2, menu &Menu,barres &Barres, unsigned char *tableau)
{
int i;

	for(i=0;i<taille8*taille;i++)
	{
		map[i]=0;
		map2[i]=0;
	}

	FILE* fichier = NULL;
	fopen_s(&fichier,"save.map_save", "rb");

	if(fichier==NULL)return;

	int x,y,chaine;

	chaine=sizeof(unsigned char);

	for(x=0;x<taille8;x++)
	{
		for(y=0;y<taille;y++)
		{
			fread(&(map[x+y*taille8]),chaine,1,fichier);
		}
	}

	fclose(fichier);

	for(i=0;i<taille8*taille8;i++)
	{
		p[i]=0;
		q[i]=0;
	}

	for(i=0;i<taille64*taille64;i++)
	{
		r[i]=0;
		s[i]=0;
	}

	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapBuffers();
	glClear(GL_COLOR_BUFFER_BIT);

	Afficher_Grille(map, pos_x, pos_y,val);
	Menu.afficher(val, pos_x, pos_y, indice2, tableau);
	Barres.afficher(val, pos_x, pos_y);

	SDL_GL_SwapBuffers();
	SDL_Delay(1000);
	glCopyPixels (0,0,screensize+texture_menu+hauteur_barre,screensize+hauteur_barre,GL_COLOR);

	Constr_de_plan2(map,p,taille);
	Constr_de_plan2(map,q,taille);
	Constr_de_plan(p,r,taille8);
}

void menu::affichage_primaire()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texture);
	glCopyTexSubImage2D(GL_TEXTURE_2D,0,0,0,xmin,ymin,xmax-xmin,ymax-ymin);
	glDisable(GL_TEXTURE_2D);
}

void Save(unsigned char* map)
{
	FILE* fichier = NULL;
	fopen_s(&fichier,"save.map_save", "wb");
	int x,y,s;

	s=sizeof(unsigned char);

	for(x=0;x<taille8;x++)
	{
		for(y=0;y<taille;y++)
		{
			fwrite(&(map[x+y*taille8]),s,1,fichier);
		}
	}

	fclose(fichier);
}

void Deplacer_Barre_horizontale(barres &Barre, menu &Menu,unsigned int X, float val, float &pos_x, float &pos_y,int indice2, unsigned char* tableau, unsigned char *map)
{
	//X abs du point dans l'ecran physique
	//centre de la barre

	//largeur de la barre
	float l=(screensize-2*hauteur_barre)/val*screensize/taille;

	//debut et fin de la barre
	int b=X-l/2;
	int c=X+l/2;

	/*if(b<17 || c>screensize-hauteur_barre-1)
	{
		return;
	}*/

	//mieux : on tronque, on ne bloque pas
	if(b<=hauteur_barre)
	{
		c+=hauteur_barre+1-b;
		b=hauteur_barre+1;
	}

	if(c>screensize-hauteur_barre)
	{
		b+=screensize-hauteur_barre-c;
		c=screensize-hauteur_barre;
	}
	
	//cf fonction afficher_barres
	int a=hauteur_barre;
	int d=screensize-hauteur_barre;

	//cf fonction afficher_barres
	//int b1=(d-a)*pos_x/(taille-1)+a;

	//position du debut de l'ecran
	pos_x=(b-a)/((float)(d-a))*(taille-1);

	Projection(val, pos_x, pos_y);

	Afficher_Grille(map, pos_x, pos_y,val);
	Menu.afficher(val, pos_x, pos_y, indice2, tableau);
	Barre.afficher(val, pos_x, pos_y);
}

void Deplacer_Barre_verticale(barres &Barre,menu &Menu,unsigned int Y, float val, float &pos_x, float &pos_y,int indice2, unsigned char* tableau, unsigned char *map)
{
	//X abs du point dans l'ecran physique
	//centre de la barre

	//largeur de la barre
	float l=(screensize-2*hauteur_barre)/val*screensize/taille;

	//debut et fin de la barre
	int b=Y-l/2;
	int c=Y+l/2;

	/*if(b<17 || c>screensize-hauteur_barre-1)
	{
		return;
	}*/

	//mieux : on tronque, on ne bloque pas
	if(b<=hauteur_barre)
	{
		c+=hauteur_barre+1-b;
		b=hauteur_barre+1;
	}

	if(c>screensize-hauteur_barre)
	{
		b+=screensize-hauteur_barre-c;
		c=screensize-hauteur_barre;
	}
	
	//cf fonction afficher_barres
	int a=hauteur_barre;
	int d=screensize-hauteur_barre;

	//cf fonction afficher_barres
	//int b2=(d-a)*pos_y/(taille-1)+a;

	//position du debut de l'ecran
	pos_y=(b-a)/((float)(d-a))*(taille-1);

	Projection(val, pos_x, pos_y);

	Afficher_Grille(map, pos_x, pos_y,val);
	Menu.afficher(val, pos_x, pos_y, indice2, tableau);
	Barre.afficher(val, pos_x, pos_y);
}
