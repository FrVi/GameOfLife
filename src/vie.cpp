#include "vie.h"
#include "structures.h"

void boucle(unsigned char *map, unsigned char *map2,unsigned int xmin,unsigned int xmax, unsigned int ymin, unsigned int ymax,unsigned char *plan,unsigned char *Plan,unsigned char *Plan2)
{
	char g1,g2,gtemp;  
	unsigned int x,y;
	char i;
	char xminC=0,xmaxC=0,yminC=0,ymaxC=0; 
	char val=0;

	if(xmin<=1){xmin=2;}
	else if(xmax>=taille-2){xmax=taille-3;}  
	if(ymin<=1){ymin=2;}
	else if(ymax>=taille-2){ymax=taille-3;}   
  

	for (y = ymin ; y <= ymax ; y++)
	{
		map2[(xmin>>3)+(taille8)*y]=0; //ameliorable (vider les cases de map2 qui perdent le focus entre 2 etats)
		g1=BinVal(map,xmin-1,y)+BinVal(map,xmin-1,y+1)+BinVal(map,xmin-1,y-1);
		g2=BinVal(map,xmin,y)+BinVal(map,xmin,y+1)+BinVal(map,xmin,y-1);

		for (x = xmin ; x <= xmax ; x++)
		{  
			gtemp=BinVal(map,x+1,y)+BinVal(map,x+1,y+1)+BinVal(map,x+1,y-1) ;   
			i=gtemp+BinVal(map,x,y-1)+BinVal(map,x,y+1)+g1; 
			g1=g2;
			g2=gtemp;  


			if( BinVal(map,x,y)==0)
			{
				if(i==3)
				{    
					Bin_1(map2,x,y);
					val=1;            
					setPixel( x, y, 255); 
				}//else case � O reste � O, pixel ne change pas
			}
			else
			{
				if(i==3 || i==2)
				{
					Bin_1(map2,x,y);
					val=1;  
				}
				else
				{
					setPixel( x, y, 0); 
				}
			}

			if( BinVal(map,x,y)!=0)
			{  
				if(x==xmin){xminC=1;}
				else if(x==xmax){xmaxC=1;}
				if(y==ymin){yminC=1;}
				else if(y==ymax){ymaxC=1;} 
			}  
		}	
	}

	Plan[(xmin>>3)+(ymin>>3)*taille8]=val; 
	Plan2[(xmin>>6)+(ymin>>6)*taille64]= Plan2[(xmin>>6)+(ymin>>6)*taille64]|val;
    
	if(xmin>7 && xminC==1)
	{                 
		if(plan[((xmin-1)>>3)+(ymin>>3)*taille8]==0)
		{
			Plan[((xmin-1)>>3)+(ymin>>3)*taille8]=Plan[((xmin-1)>>3)+(ymin>>3)*taille8]|Up_Down(map, map2,ymin,ymax,xmin-1);  
			Plan2[(((xmin>>3)-1)>>3)+(ymin>>6)*taille64]=1;    
		}               
	}
  
	if(xmax<taille-9&& xmaxC==1)
	{                   
		if(plan[((xmax)>>3)+1  +(ymin>>3)*taille8]==0)
		{
			Plan[((xmax)>>3)+1  +(ymin>>3)*taille8]= Plan[((xmax)>>3)+1  +(ymin>>3)*taille8]|Up_Down(map, map2,ymin,ymax,xmax+1); 
			Plan2[(((xmax>>3)+1)>>3)+(ymin>>6)*taille64]=1;                                                                          
		}          
	}
 
	if(ymin>7 && yminC==1)
	{                 
	if(plan[((xmin)>>3)+((ymin-1)>>3)*taille8]==0)
		{
			Plan[((xmin)>>3)+((ymin-1)>>3)*taille8]=Plan[((xmin)>>3)+((ymin-1)>>3)*taille8]|Left_Right(map, map2,xmin,xmax,ymin-1);  
			Plan2[(xmin>>6)+(((ymin>>3)-1)>>3)*taille64]=1;    
		}               
	} 

	if(ymax<taille-9 && ymaxC==1)
	{                               
		if(plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]==0)
		{
			Plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]=Plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]|Left_Right(map, map2,xmin,xmax,ymax+1); 
			Plan2[(xmin>>6)+(((ymax>>3)+1)>>3)*taille64]=1;                                                                        
		}        
	}
}
                                                             
unsigned char Left_Right(unsigned char *map, unsigned char *map2,unsigned int xmin,unsigned int xmax, unsigned int y)                                       
{ 
	unsigned char  val;
	unsigned int x;
	int i ;   
    char g1,g2,gtemp;                                          
	val=0;
  
	g1=BinVal(map,xmin-1,y)+BinVal(map,xmin-1,y+1)+BinVal(map,xmin-1,y-1);
	g2=BinVal(map,xmin,y)+BinVal(map,xmin,y+1)+BinVal(map,xmin,y-1);
    
	for(x=xmin;x<=xmax;x++) 
    {
		gtemp=BinVal(map,x+1,y)+BinVal(map,x+1,y+1)+BinVal(map,x+1,y-1) ;   
        i=gtemp+BinVal(map,x,y-1)+BinVal(map,x,y+1)+g1; 
		g1=g2;
		g2=gtemp; 

		if (i==2)
		{    
		   val=val| BinVal(map,x,y);  
           BinChg(map2,x,y,BinVal(map,x,y));  
		}
		else if(i==3)
		{    
			Bin_1(map2,x,y);  
			val=1;                    
			setPixel( x, y, 255); 
		}
		else 
		{    
			Bin_0(map2,x,y);  
			setPixel( x, y, 0);     
		}
   }  
   return val;
}

unsigned char Up_Down(unsigned char *map, unsigned char *map2,unsigned int ymin,unsigned int ymax, unsigned int x)                                       
{ 

	unsigned char val;
    unsigned int y;
    int i ; 
	val=0;
	char g1,g2,gtemp;
  
	g1=BinVal(map,x,ymin-1)+BinVal(map,x+1,ymin-1)+BinVal(map,x-1,ymin-1);
	g2=BinVal(map,x,ymin)+BinVal(map,x+1,ymin)+BinVal(map,x-1,ymin);

	for(y=ymin;y<=ymax;y++)
    {
		gtemp=BinVal(map,x,y+1)+BinVal(map,x+1,y+1)+BinVal(map,x-1,y+1) ;   
        i=gtemp+BinVal(map,x-1,y)+BinVal(map,x+1,y)+g1; 
		g1=g2;
		g2=gtemp;                      

       if (i==2)
       {          
			val=val| BinVal(map,x,y);                
			BinChg(map2,x,y,BinVal(map,x,y)); 
       }
       else if(i==3)
       {    
			Bin_1(map2,x,y); 
			val=1;                    
			setPixel( x, y, 255); 
		}
		else 
		{    
			Bin_0(map2,x,y); 
			setPixel( x, y, 0);     
		}
	}  
	return val;
}
 
void fun2(int x, int y, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	int i;

	int g1=BinVal(*map,x-1,y)+BinVal(*map,x-1,y+1)+BinVal(*map,x-1,y-1);
	int g2=BinVal(*map,x,y)+BinVal(*map,x,y+1)+BinVal(*map,x,y-1);
	int gtemp;

	int val;

	for(i=0;i<8;i++)
	{
		gtemp=BinVal(*map,x+1,y)+BinVal(*map,x+1,y+1)+BinVal(*map,x+1,y-1) ;   
		val=gtemp+BinVal(*map,x,y-1)+BinVal(*map,x,y+1)+g1; 
		g1=g2;
		g2=gtemp;  

	if(val<2 || val>3)
	{
		Bin_0(*map2,x,y);
		setPixel( x, y, 0); 
	}
	else if(val==2)
	{
		if(BinVal(*map,x,y)!=0)
		{
			Bin_1(*map2,x,y);
		}
		else
		{
			Bin_0(*map2,x,y);
		}
	}
	else
	{
		Bin_1(*map2,x,y);
		setPixel( x, y, 255); 
	}
		x++;
	}
}

void fun3(int xmin, int ymin, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	int x,y,g1,g2;

	int gtemp;

	int val;
	for(y=ymin;y<ymin+8;y++)
	{
		g1=BinVal(*map,xmin-1,y)+BinVal(*map,xmin-1,y+1)+BinVal(*map,xmin-1,y-1);
		g2=BinVal(*map,xmin,y)+BinVal(*map,xmin,y+1)+BinVal(*map,xmin,y-1);

		for(x=xmin;x<xmin+8;x++)
		{
			gtemp=BinVal(*map,x+1,y)+BinVal(*map,x+1,y+1)+BinVal(*map,x+1,y-1) ;   
			val=gtemp+BinVal(*map,x,y-1)+BinVal(*map,x,y+1)+g1; 
			g1=g2;
			g2=gtemp;  

			if(val<2 || val>3)
			{
				Bin_0(*map2,x,y);
				setPixel( x, y, 0); 
			}
			else if(val==2)
			{
				if(BinVal(*map,x,y)!=0)
				{
					Bin_1(*map2,x,y);
				}
				else
				{
					Bin_0(*map2,x,y);
				}
			}
			else
			{
				Bin_1(*map2,x,y);
				setPixel( x, y, 255); 
			}
		}
	}
}

void fun(int x, int y, unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	int val;

	val = BinVal(*map,x,y-1) + BinVal(*map,x,y+1)
	    + BinVal(*map,x-1,y-1) + BinVal(*map,x-1,y) + BinVal(*map,x-1,y+1)
		+ BinVal(*map,x+1,y-1) + BinVal(*map,x+1,y) + BinVal(*map,x+1,y+1);

	if(val<2 || val>3)
	{
		Bin_0(*map2,x,y);
		setPixel( x, y, 0); 
	}
	else if(val==2)
	{
		if(BinVal(*map,x,y)!=0)
		{
			Bin_1(*map2,x,y);
		}
		else
		{
			Bin_0(*map2,x,y);
		}
	}
	else
	{
		Bin_1(*map2,x,y);
		setPixel( x, y, 255); 
	}
}

//version simple
void Game_Of_Life_v0(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	unsigned char* tr;

	int x,y;

	int val;

	//ignore les bords
	for(x=1;x<taille-1;x++)
	{
		for(y=1;y<taille-1;y++)
		{
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
		}
	}

	tr = *map;  *map = *map2;  *map2 = tr;

	//Update(temps_P,mode);
}

//version simple
void Game_Of_Life_v0_5(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	unsigned char* tr;

	int x,y;

	int val;

	//fait les bords � la bourrin
	for(y=1;y<taille-1;y++)
	{
		for(x=1;x<8;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(x=taille-8;x<taille-1;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}

	for(x=8;x<taille-8;x++)
	{
		for(y=1;y<8;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(y=taille-8;y<taille-1;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}


	for(x=1;x<taille8-1;x++)
	{
		for(y=1;y<taille-1;y++)
		{
			fun2((x<<3), y,  p, q, r, s, map, map2,temps_P,mode);
		}
	}

	for(x=0;x<taille8;x++)
	{
		for(y=0;y<taille;y++)
		{	
			 (*map)[x+y*taille8]=0 ;  
		}
	}

	tr = *map;  *map = *map2;  *map2 = tr;

	//Update(temps_P,mode);
}

//cuda prototype
void Game_Of_Life_v4(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	unsigned char* tr;

	int x,y,i,j;

	int val;

	//fait les bords � la bourrin
	//CPU
	//{
	for(y=1;y<taille-1;y++)
	{
		for(x=1;x<8;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(x=taille-8;x<taille-1;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}

	for(x=8;x<taille-8;x++)
	{
		for(y=1;y<8;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(y=taille-8;y<taille-1;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}
	//}
	//CPU


	//vraie boucle for
for(i=0;i<2;i++)
{
	for(j=0;j<2;j++)
	{
		//GPU GRID
		//{
		for(x=0;x<taille16-1;x++)
		{
			for(y=0;y<taille16-1;y++)
			{

				//un thread
				//{
				fun3((((x<<1)+1+i)<<3), (((y<<1)+1+j)<<3),  p, q, r, s, map, map2,temps_P,mode);
				//}
			}
		}
		//}

	}
}

	//GPU GRID
	for(x=0;x<taille8;x++)
	{
		for(y=0;y<taille8;y++)
		{	
			//un thread
			//{
			for(i=0;i<8;i++)
			{
				(*map)[x+((y<<3)+i)*taille8]=0 ; 
			}
			//}
		}
	}

	tr = *map;  *map = *map2;  *map2 = tr;

	//Update(temps_P,mode);
}

//version quadtree
void Game_Of_Life_v1(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{

	unsigned char* tr;
	int x,y,x2,y2;

	for(y=0 ; y<=taille64-1 ; y++)
	for(x=0 ; x<=taille64-1 ; x++)       
	{      
		if((*r)[x+y*taille64]!=0)
		{ 
			for(y2=8*y ; y2<=7+8*y ; y2++)
            for(x2=8*x ; x2<=7+8*x ; x2++)       
            {         
				if((*p)[x2+y2*taille8]!=0){  boucle( *map,*map2,x2*8,x2*8+7,y2*8,y2*8+7,*p,*q,*s); } 
            }
        }
	}

	for(y=0 ; y<=taille64-1 ; y++)
	for(x=0 ; x<=taille64-1 ; x++)       
	{      
		if((*r)[x+y*taille64]!=0)//si la grande case �tait pleine
        { 
			for(y2=8*y ; y2<=7+8*y ; y2++) //alors il est possible qu'il faille vider une petite case 
			for(x2=8*x ; x2<=7+8*x ; x2++) //(si elle etait vide les petites aussi)       
            {               
				if((*q)[x2+y2*taille8]==0 && (*p)[x2+y2*taille8]!=0)
				{
					VIDER(x2*8,x2*8+7,y2*8,y2*8+7,*map);
					(*p)[x2+y2*taille8]=0;
				}  
            } 
		}	
	}

	for(y=0;y<taille64;y++)//vide les grandes cases de l'etat precedent              
	for(x=0;x<taille64;x++)
	{
		(*r)[x+y*taille64]=0;
	}

	tr = *p;	    *p = *q;	       *q = tr;
	tr = *r;	     *r = *s;	       *s = tr;  
	tr = *map;  *map = *map2;  *map2 = tr;

	//Update(temps_P,mode);
}

void Funct(unsigned char* map,unsigned int x,unsigned int y,unsigned int val_case,unsigned int change,char &val)
{
	if(change!=1)
	{
		if (val_case==1)val=1;

	}
	else
	{
		if(val_case==1)
		{
				setPixel( x, y, 255); 
				val=1;
		}
		else
		{
				setPixel( x, y, 0); 
		}
	}
}

void Funct(unsigned char* map,unsigned int x,unsigned int y,unsigned int val_case,unsigned int change)
{
	if(change==1)
	{
		if(val_case==1)
		{
				setPixel( x, y, 255); 
		}
		else
		{
				setPixel( x, y, 0); 
		}
	}
}

//version sans calcul ou presque
void Game_Of_Life_v2(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	/*FILE* fichier = NULL;
	fopen_s(&fichier,"result.txt", "w");*/

	unsigned char* tr;

	int x,y;

	int val;

	//fait les bords � la bourrin
	for(y=1;y<taille-1;y++)
	{
		for(x=1;x<8;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(x=taille-8;x<taille-1;x++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}

	for(x=8;x<taille-8;x++)
	{
		for(y=1;y<8;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);

		for(y=taille-8;y<taille-1;y++)
			fun(x, y,  p, q, r, s, map, map2,temps_P,mode);
	}

	unsigned char tab4_1[10]={0};
	unsigned char tab4_3[10]={0};
	unsigned char tab3_0[10]={0};
	unsigned char tab3_6[10]={0};
	
	int i;
	unsigned int a;
	unsigned int temp;
	unsigned int x2, y2;

	unsigned char lign[2];

	int j,k;

	for(x=1;x<taille8-1;x++)
	{
		for(y=1;y<taille8-1;y++)
		{

			x2=(x<<3);
			y2=(y<<3);
			
			for(i=-1;i<9;i++)
			{
				temp=(*map)[x+taille8*(y2+i)];
				tab4_3[i+1]= ((temp>>3)&15);
				tab4_1[i+1]= ((temp>>1)&15);

				tab3_0[i+1]=(((temp&7)<<1)|(((*map)[x-1+taille8*(y2+i)])>>7));
				tab3_6[i+1]=((temp>>5)|((((*map)[x+1+taille8*(y2+i)])&1)<<3));

			}

	
			for(i=0;i<7;i=i+2)
			{	
				a=(tab3_0[i]<<12)
				 |(tab3_0[i+1]<<8)
				 |(tab3_0[i+2]<<4)
				 |(tab3_0[i+3]<<0);

				temp = index[a];

				Funct(*map2,x2,y2+i,BinV(temp,0),BinV(temp,7));
				Funct(*map2,x2+1,y2+i,BinV(temp,1),BinV(temp,6));
				Funct(*map2,x2,y2+1+i,BinV(temp,2),BinV(temp,5));
				Funct(*map2,x2+1,y2+1+i,BinV(temp,3),BinV(temp,4));

				lign[0]=(temp&3);
				lign[1]=((temp>>2)&3);


				a=(tab4_1[i]<<12)
				 |(tab4_1[i+1]<<8)
				 |(tab4_1[i+2]<<4)
				 |(tab4_1[i+3]<<0);

				temp = index[a];

				Funct(*map2,x2+2,y2+i,BinV(temp,0),BinV(temp,7));
				Funct(*map2,x2+3,y2+i,BinV(temp,1),BinV(temp,6));
				Funct(*map2,x2+2,y2+1+i,BinV(temp,2),BinV(temp,5));
				Funct(*map2,x2+3,y2+1+i,BinV(temp,3),BinV(temp,4));

				lign[0]=lign[0]|((temp&3)<<2);
				lign[1]=lign[1]|(((temp>>2)&3)<<2);

				a=(tab4_3[i]<<12)
				 |(tab4_3[i+1]<<8)
				 |(tab4_3[i+2]<<4)
				 |(tab4_3[i+3]<<0);

				temp = index[a];

				Funct(*map2,x2+4,y2+i,BinV(temp,0),BinV(temp,7));
				Funct(*map2,x2+5,y2+i,BinV(temp,1),BinV(temp,6));
				Funct(*map2,x2+4,y2+1+i,BinV(temp,2),BinV(temp,5));
				Funct(*map2,x2+5,y2+1+i,BinV(temp,3),BinV(temp,4));

				lign[0]=lign[0]|((temp&3)<<4);
				lign[1]=lign[1]|(((temp>>2)&3)<<4);


				a=(tab3_6[i]<<12)
				 |(tab3_6[i+1]<<8)
				 |(tab3_6[i+2]<<4)
				 |(tab3_6[i+3]<<0);

				temp = index[a];

				Funct(*map2,x2+6,y2+i,BinV(temp,0),BinV(temp,7));
				Funct(*map2,x2+7,y2+i,BinV(temp,1),BinV(temp,6));
				Funct(*map2,x2+6,y2+1+i,BinV(temp,2),BinV(temp,5));
				Funct(*map2,x2+7,y2+1+i,BinV(temp,3),BinV(temp,4));

				(*map2)[x+(taille8)*(y2+i)]=(lign[0]|((temp&3)<<6));
				(*map2)[x+(taille8)*(y2+i+1)]=(lign[1]|((temp>>2)&3)<<6);
			}
		}
	}

	tr = *map;  *map = *map2;  *map2 = tr;

	//Update(temps_P,mode);
	//fclose(fichier);
}

void boucle2(unsigned short *index, unsigned char *map, unsigned char *map2,unsigned int xmin,unsigned int xmax, unsigned int ymin, unsigned int ymax,unsigned char *plan,unsigned char *Plan,unsigned char *Plan2)
{
	char g1,g2,gtemp;  
	unsigned int x,y;
	char i;
	char xminC=0,xmaxC=0,yminC=0,ymaxC=0; 
	char val=0;
  
if(ymax<8 || ymin>taille-9 || xmax<8 || xmin>taille-9)
{
	if(xmin<=1){xmin=2;}
	else if(xmax>=taille-2){xmax=taille-3;}  
	if(ymin<=1){ymin=2;}
	else if(ymax>=taille-2){ymax=taille-3;}   

	for (y = ymin ; y <= ymax ; y++)
	{
		map2[(xmin>>3)+(taille8)*y]=0; //ameliorable (vider les cases de map2 qui perdent le focus entre 2 etats)
		g1=BinVal(map,xmin-1,y)+BinVal(map,xmin-1,y+1)+BinVal(map,xmin-1,y-1);
		g2=BinVal(map,xmin,y)+BinVal(map,xmin,y+1)+BinVal(map,xmin,y-1);

		for (x = xmin ; x <= xmax ; x++)
		{  
			gtemp=BinVal(map,x+1,y)+BinVal(map,x+1,y+1)+BinVal(map,x+1,y-1) ;   
			i=gtemp+BinVal(map,x,y-1)+BinVal(map,x,y+1)+g1; 
			g1=g2;
			g2=gtemp;  


			if( BinVal(map,x,y)==0)
			{
				if(i==3)
				{    
					Bin_1(map2,x,y);
					val=1;            
					setPixel( x, y, 255); 
				}//else case � O reste � O, pixel ne change pas
			}
			else
			{
				if(i==3 ||i==2)
				{
					Bin_1(map2,x,y);
					val=1;  
				}
				else
				{
					setPixel( x, y, 0); 
				}
			}

			if( BinVal(map,x,y)!=0)
			{  
				if(x==xmin){xminC=1;}
				else if(x==xmax){xmaxC=1;}
				if(y==ymin){yminC=1;}
				else if(y==ymax){ymaxC=1;} 
			}  
		}	
	}
}
else
{
	unsigned char tab4_1[10]={0};
	unsigned char tab4_3[10]={0};
	unsigned char tab3_0[10]={0};
	unsigned char tab3_6[10]={0};
	
	int i;
	unsigned int a;
	unsigned int temp;
	unsigned int x2, y2;

	unsigned char lign[2];

	int j,k;

	x=xmin>>3;

	for(i=ymin;i<ymax;i++)
	{	
		map2[x+(taille8)*i]=0;
	}
			
	for(i=-1;i<9;i++)
	{
		temp=map[x+taille8*(ymin+i)];
		tab4_3[i+1]= ((temp>>3)&15);
		tab4_1[i+1]= ((temp>>1)&15);

		tab3_0[i+1]=(((temp&7)<<1)|((map[x-1+taille8*(ymin+i)])>>7));
		tab3_6[i+1]=((temp>>5)|(((map[x+1+taille8*(ymin+i)])&1)<<3));
	}

	
	for(i=0;i<7;i=i+2)
	{	
		a=(tab3_0[i]<<12)
		 |(tab3_0[i+1]<<8)
		 |(tab3_0[i+2]<<4)
		 |(tab3_0[i+3]<<0);

		temp = index[a];

		Funct(map2,xmin,ymin+i,BinV(temp,0),BinV(temp,7),val);
		Funct(map2,xmin+1,ymin+i,BinV(temp,1),BinV(temp,6),val);
		Funct(map2,xmin,ymin+1+i,BinV(temp,2),BinV(temp,5),val);
		Funct(map2,xmin+1,ymin+1+i,BinV(temp,3),BinV(temp,4),val);

		lign[0]=(temp&3);
		lign[1]=((temp>>2)&3);


		a=(tab4_1[i]<<12)
		 |(tab4_1[i+1]<<8)
		 |(tab4_1[i+2]<<4)
		 |(tab4_1[i+3]<<0);

		temp = index[a];

		Funct(map2,xmin+2,ymin+i,BinV(temp,0),BinV(temp,7),val);
		Funct(map2,xmin+3,ymin+i,BinV(temp,1),BinV(temp,6),val);
		Funct(map2,xmin+2,ymin+1+i,BinV(temp,2),BinV(temp,5),val);
		Funct(map2,xmin+3,ymin+1+i,BinV(temp,3),BinV(temp,4),val);


		lign[0]=lign[0]|((temp&3)<<2);
		lign[1]=lign[1]|(((temp>>2)&3)<<2);

		a=(tab4_3[i]<<12)
		 |(tab4_3[i+1]<<8)
		 |(tab4_3[i+2]<<4)
		 |(tab4_3[i+3]<<0);

		temp = index[a];


		Funct(map2,xmin+4,ymin+i,BinV(temp,0),BinV(temp,7),val);
		Funct(map2,xmin+5,ymin+i,BinV(temp,1),BinV(temp,6),val);
		Funct(map2,xmin+4,ymin+1+i,BinV(temp,2),BinV(temp,5),val);
		Funct(map2,xmin+5,ymin+1+i,BinV(temp,3),BinV(temp,4),val);

		lign[0]=lign[0]|((temp&3)<<4);
		lign[1]=lign[1]|(((temp>>2)&3)<<4);


		a=(tab3_6[i]<<12)
		 |(tab3_6[i+1]<<8)
		 |(tab3_6[i+2]<<4)
		 |(tab3_6[i+3]<<0);

		temp = index[a];


		Funct(map2,xmin+6,ymin+i,BinV(temp,0),BinV(temp,7),val);
		Funct(map2,xmin+7,ymin+i,BinV(temp,1),BinV(temp,6),val);
		Funct(map2,xmin+6,ymin+1+i,BinV(temp,2),BinV(temp,5),val);
		Funct(map2,xmin+7,ymin+1+i,BinV(temp,3),BinV(temp,4),val);


		map2[x+(taille8)*(ymin+i)]=(lign[0]|((temp&3)<<6));
		map2[x+(taille8)*(ymin+i+1)]=(lign[1]|((temp>>2)&3)<<6);
	}

	for(i=ymin;i<ymax;i++)
	{
		if( BinVal(map,xmin,i)!=0)
		{  
			xminC=1;
			if(i==ymin){yminC=1;}
			else if(i==ymax){ymaxC=1;} 
		}  
		if( BinVal(map,xmax,i)!=0)
		{  
			xmaxC=1;
			if(i==ymin){yminC=1;}
			else if(i==ymax){ymaxC=1;} 
		}  
	}

	for(i=xmin;i<xmax;i++)
	{
		if( BinVal(map,i,ymin)!=0)
		{  
			yminC=1;
			if(i==xmin){xminC=1;}
			else if(i==xmax){xmaxC=1;} 
		}  
		if( BinVal(map,i,ymax)!=0)
		{  
			ymaxC=1;
			if(i==xmin){xminC=1;}
			else if(i==xmax){xmaxC=1;} 
		}  
	}

}
/*
for (y = ymin ; y <= ymax ; y++)
{
		for (x = xmin ; x <= xmax ; x++)
		{  
			if(BinVal(map,x,y)==1)
			{
				setPixel( x, y, 255); 
			}
			else
			{
				setPixel( x, y, 0); 
			}
		}

}*/

Plan[(xmin>>3)+(ymin>>3)*taille8]=val; 
Plan2[(xmin>>6)+(ymin>>6)*taille64]= Plan2[(xmin>>6)+(ymin>>6)*taille64]|val;
    
	if(xmin>7 && xminC==1)
	{                 
		if(plan[((xmin-1)>>3)+(ymin>>3)*taille8]==0)
		{
			Plan[((xmin-1)>>3)+(ymin>>3)*taille8]=Plan[((xmin-1)>>3)+(ymin>>3)*taille8]|Up_Down(map, map2,ymin,ymax,xmin-1);  
			Plan2[(((xmin>>3)-1)>>3)+(ymin>>6)*taille64]=1;    
		}               
	}
  
	if(xmax<taille-9&& xmaxC==1)
	{                   
		if(plan[((xmax)>>3)+1  +(ymin>>3)*taille8]==0)
		{
			Plan[((xmax)>>3)+1  +(ymin>>3)*taille8]= Plan[((xmax)>>3)+1  +(ymin>>3)*taille8]|Up_Down(map, map2,ymin,ymax,xmax+1); 
			Plan2[(((xmax>>3)+1)>>3)+(ymin>>6)*taille64]=1;                                                                          
		}          
	}
 
	if(ymin>7 && yminC==1)
	{                 
	if(plan[((xmin)>>3)+((ymin-1)>>3)*taille8]==0)
		{
			Plan[((xmin)>>3)+((ymin-1)>>3)*taille8]=Plan[((xmin)>>3)+((ymin-1)>>3)*taille8]|Left_Right(map, map2,xmin,xmax,ymin-1);  
			Plan2[(xmin>>6)+(((ymin>>3)-1)>>3)*taille64]=1;    
		}               
	} 

	if(ymax<taille-9 && ymaxC==1)
	{                               
		if(plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]==0)
		{
			Plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]=Plan[(xmin>>3)+(((ymax)>>3)+1)*taille8]|Left_Right(map, map2,xmin,xmax,ymax+1); 
			Plan2[(xmin>>6)+(((ymax>>3)+1)>>3)*taille64]=1;                                                                        
		}        
	}
}

//version quadtree sans calcul
void Game_Of_Life_v3(unsigned short *index,unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{

	unsigned char* tr;
	int x,y,x2,y2;

	for(y=0 ; y<=taille64-1 ; y++)
	for(x=0 ; x<=taille64-1 ; x++)       
	{      
		if((*r)[x+y*taille64]!=0)
		{ 
			for(y2=8*y ; y2<=7+8*y ; y2++)
            for(x2=8*x ; x2<=7+8*x ; x2++)       
            {         
				if((*p)[x2+y2*taille8]!=0){  boucle2(index, *map,*map2,x2*8,x2*8+7,y2*8,y2*8+7,*p,*q,*s); } 
            }
        }
	}

	for(y=0 ; y<=taille64-1 ; y++)
	for(x=0 ; x<=taille64-1 ; x++)       
	{      
		if((*r)[x+y*taille64]!=0)//si la grande case �tait pleine
        { 
			for(y2=8*y ; y2<=7+8*y ; y2++) //alors il est possible qu'il faille vider une petite case 
			for(x2=8*x ; x2<=7+8*x ; x2++) //(si elle etait vide les petites aussi)       
            {               
				if((*q)[x2+y2*taille8]==0 && (*p)[x2+y2*taille8]!=0)
				{
					VIDER(x2*8,x2*8+7,y2*8,y2*8+7,*map);
					(*p)[x2+y2*taille8]=0;
				}  
            } 
		}	
	}

	for(y=0;y<taille64;y++)//vide les grandes cases de l'etat precedent              
	for(x=0;x<taille64;x++)
	{
		(*r)[x+y*taille64]=0;
	}

	tr = *p;	    *p = *q;	       *q = tr;
	tr = *r;	     *r = *s;	       *s = tr;  
	tr = *map;  *map = *map2;  *map2 = tr;
}


void init_index(unsigned short index[65535],unsigned char** p, unsigned char** q, unsigned char** r, unsigned char** s, unsigned char** map, unsigned char** map2,int *temps_P, int mode)
{
	unsigned int i;
	unsigned short temp;

	FILE* fichier = NULL;
	fopen_s(&fichier,"table.txt", "w");
	
	for(i=0;i<65536;i++)
	{
		BinChg(*map,10,10,BinV(i,12));
		BinChg(*map,11,10,BinV(i,13));
		BinChg(*map,12,10,BinV(i,14));
		BinChg(*map,13,10,BinV(i,15));
		BinChg(*map,10,11,BinV(i,8));
		BinChg(*map,11,11,BinV(i,9));
		BinChg(*map,12,11,BinV(i,10));
		BinChg(*map,13,11,BinV(i,11));
		BinChg(*map,10,12,BinV(i,4));
		BinChg(*map,11,12,BinV(i,5));
		BinChg(*map,12,12,BinV(i,6));
		BinChg(*map,13,12,BinV(i,7));
		BinChg(*map,10,13,BinV(i,0));
		BinChg(*map,11,13,BinV(i,1));
		BinChg(*map,12,13,BinV(i,2));
		BinChg(*map,13,13,BinV(i,3));

		fun(11, 11,  p, q, r, s, map, map2,temps_P,mode);
		fun(11, 12,  p, q, r, s, map, map2,temps_P,mode);
		fun(12, 11,  p, q, r, s, map, map2,temps_P,mode);
		fun(12, 12,  p, q, r, s, map, map2,temps_P,mode);

		//fprintf(fichier,"%hu\n",((BinVal(*map2,11,11)<<3)|(BinVal(*map2,11,12)<<2)|(BinVal(*map2,12,11)<<1)|BinVal(*map2,12,12)));
		temp=0;

		//changement d'�tat : pixel � mettre � jour
		if(BinVal(*map2,11,11)!=BinVal(*map,11,11))		temp|=(1<<7);
		if(BinVal(*map2,12,11)!=BinVal(*map,12,11))		temp|=(1<<6);
		if(BinVal(*map2,11,12)!=BinVal(*map,11,12))		temp|=(1<<5);
		if(BinVal(*map2,12,12)!=BinVal(*map,12,12))		temp|=(1<<4);

		index[i]=(temp|(BinVal(*map2,11,11)<<0)|(BinVal(*map2,12,11)<<1)|(BinVal(*map2,11,12)<<2)|(BinVal(*map2,12,12)<<3));
		fprintf(fichier,"%hu\n",index[i]);
	}

	fclose(fichier);
}

bool load_index(unsigned short index[65535])
{
	FILE* fichier = NULL;
	fopen_s(&fichier,"table.txt", "r");

	if(fichier == NULL) return false;

	for(int i=0;i<65536;i++)
	{
		fscanf(fichier,"%hu",&(index[i]));
	}

	fclose(fichier);
	return true;
}

/*
bigger spaceship

00111
01001
00001
10001
00001
01010	


other

00111
01001
00001
10001
10001
00001
01010

*/



  /* 
for(y=0;y<taille8;y++)
{               
for(x=0;x<taille8;x++)
{
 if(q[x+y*64]==0){FullBox(x*8,x*8+7,y*8,y*8+7,0x00ff00);}  
                }
                } 
                
        */       
                
/*
for(y=0;y<taille64;y++)
{               
for(x=0;x<taille64;x++)
{
FullBox(x*64,x*64+63,y*64,y*64+63,0); 

                 }
                 } 
*/
/*
for(y=0;y<taille64;y++)
{               
for(x=0;x<taille64;x++)
{
 if(s[x+y*taille64]==0){FullBox(x*64,x*64+63,y*64,y*64+63,0x0000ff);} 

                 }
                 } 
                    
        
      SDL_UpdateRect(0,0,0,0);  
        
       
        
 */   


/*
//methode rapide: tout l'ecran mais seulement si la frame peut �tre afficher (70fps) tps pour afficher 1000/10000 etapes : (1100/26088) 
temps_A = SDL_GetTicks();
if(temps_A - temps_P >=14)
{
	temps_P=temps_A;
SDL_UpdateRect(0,0,taille,taille);

}
*/


/* 
//Methode naive: tout l'ecran � chaque frame (4974/...)
SDL_UpdateRect(0,0,taille,taille); 
*/


/* Methode partielle: zones qui changent mais � chaque frame (1521/37672) 
			for(y=0 ; y<=taille64-1 ; y++)
			{
				for(x=0 ; x<=taille64-1 ; x++)       
				{  
					if(r[x+y*taille64]!=0||s[x+y*taille64]!=0){SDL_UpdateRect(x*64,y*64,64,64);}
				}
            }
*/


/*//code pour voir si les cases sont bien vides

			for(y=0 ; y<=taille64-1 ; y++)
		    {
				for(x=0 ; x<=taille64-1 ; x++)       
				{   

                    if(r[x+y*taille64]!=0)
                    { 
                         for(y2=8*y ; y2<=7+8*y ; y2++)
                          {
                                     for(x2=8*x ; x2<=7+8*x ; x2++)       
                                     {         
                                               if(p[x2+y2*taille8]==0)
											   {	
												   FullBox(x2*8, 8*x2+7,8*y2, 8*y2+7, 0x0000ff);
											   } 
											   else
											   {
												   FullBox(x2*8, 8*x2+7,8*y2, 8*y2+7, 0x00ff00);
											   }
                                      }
                          } 
					}
					else{   FullBox(x*64, 64*x+63,64*y, 64*y+63, 0x0000ff);}
				
				}}
*/

/*
if(xmin<=7){xmin=8;}
else if(xmax>=taille-9){xmax=taille-10;}  
if(ymin<=7){ymin=8;}
else if(ymax>=taille-9){ymax=taille-10;}    
  */


/*
glider
111
100
010

*/


/*


acorn
0100000
0001000
1100111
*/

/*





glider
111
100
010

*/

/*


L_spaceship
01001
10000
10001
01110
*/

/*





Gosper_glider_gun
000000000000000000000000100000000000
000000000000000000000010100000000000
000000000000110000001100000000000011
000000000001000100001100000000000011
110000000010000010001100000000000000
110000000010000111000010100000000000
000000000010000010000000100000000000
000000000001000100000000000000000000
000000000000110000000000000000000000
*/

/*



F_pentamino
011
110
010

*/

/*



Exploder
10101
10001
10001
10001
10101
*/

/*


unknown
111
100
111
*/









