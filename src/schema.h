#include "structures.h"

#define nbre_schema 13

class schema
{
	public:
	schema(unsigned char *tab, unsigned int x, unsigned int y, char* name, bool &DEFINE);
	unsigned int get_x();
	unsigned int get_y();

	void Create_Struct(unsigned char *map,unsigned int x,unsigned int y,unsigned char *plan,unsigned char *plan2,int orientation);

	private:

	unsigned char *tab;
	unsigned int x;
	unsigned int y;  
};
