#include "bouton.h"

bouton::bouton(unsigned int x_min,unsigned int x_max,unsigned int y_min,unsigned int y_max,unsigned int valeur,char *string_texture)
{
	this->valeur=valeur;
	this->x_min=x_min;
	this->x_max=x_max;
	this->y_min=y_min;
	this->y_max=y_max;
	this->texture=loadTexture(string_texture,false);
}

void bouton::affichage(float &val, float pos_x, float pos_y)
{
	Affichage_Droit(texture, val, x_min, y_min, x_max, y_max, pos_x, pos_y);
}

bool bouton::appartient(unsigned int x, unsigned int y)
{
	bool val=(x>=x_min && x<=x_max && y>=y_min && y<=y_max);
	return val;
}

void bouton::fonction(int &mode,bool &continuer, int &indice, int &indice2, float pos_x, float pos_y, float val,int &indice3)
{
	switch (valeur) {
	case 0:
	{
		if(continuer){continuer=false;}
		else{continuer=true;}
	}
    break;
	case 1:
	{
		mode=3;
	}
    break;
	case 2:
	{
		mode=1;
	}
    break;
	case 3:
	{
		mode=2;
	}
    break;
	case 4:
	{
		indice2=(indice2)+4;
		if((indice2)>=9){indice2=(indice2)-8;}
		Orientation( indice2, pos_x, pos_y, val);
	}
    break;
	case 5:
	{
		if(indice2<=4)
		{
			indice2=indice2+1;
			if(indice2==5)	indice2=1;
		}
		else
		{
			indice2=indice2+1;
			if(indice2==9)	indice2=5;
		}
		Orientation( indice2, pos_x, pos_y, val);
	}
    break;
	case 6:
	{
		indice=7;
	}
    break;
	case 7:
	{
		indice=1;
	}
    break;
	case 8:
	{
		indice=3;
	}
    break;
	case 9:
	{
		indice=2;	
	}
    break;
	case 10:
	{
		indice=5;
	}
    break;
	case 11:
	{
		indice=6;
	}
    break;
	case 12:
	{
		indice=4;
	}
    break;
	case 13:
	{
		indice=0;
	}
    break;
	case 14:
	{
		indice=8;
	}
    break;
	case 15:
	{
		indice3=1;
	}
    break;
	case 16:
	{
		indice3=2;
	}
    break;
	case 17:
	{
		indice=9;
	}
    break;
	case 18:
	{
		indice3=3;
	}
    break;
	case 19:
	{
		indice=12;
	}
    break;
	case 20:
	{
		indice=10;
	}
    break;
	case 21:
	{
		indice=11;
	}
    break;
	default:
    throw 18;
  }

}

GLuint loadTexture(const char * filename,bool useMipMap)
{
    GLuint glID;
    SDL_Surface * picture_surface = NULL;
    SDL_Surface *gl_surface = NULL;
    SDL_Surface * gl_fliped_surface = NULL;
    Uint32 rmask, gmask, bmask, amask;

    picture_surface = SDL_LoadBMP(filename);
    if (picture_surface == NULL)
        return 0;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN

    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else

    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    SDL_PixelFormat format = *(picture_surface->format);
    format.BitsPerPixel = 32;
    format.BytesPerPixel = 4;
    format.Rmask = rmask;
    format.Gmask = gmask;
    format.Bmask = bmask;
    format.Amask = amask;

    gl_surface = SDL_ConvertSurface(picture_surface,&format,SDL_SWSURFACE);

    gl_fliped_surface = flipSurface(gl_surface);

    glGenTextures(1, &glID);

    glBindTexture(GL_TEXTURE_2D, glID);

    if (useMipMap)
    {

        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, gl_fliped_surface->w,
                          gl_fliped_surface->h, GL_RGBA,GL_UNSIGNED_BYTE,
                          gl_fliped_surface->pixels);

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);

    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, 4, gl_fliped_surface->w,
                     gl_fliped_surface->h, 0, GL_RGBA,GL_UNSIGNED_BYTE,
                     gl_fliped_surface->pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);


    SDL_FreeSurface(gl_fliped_surface);
    SDL_FreeSurface(gl_surface);
    SDL_FreeSurface(picture_surface);

    return glID;
}

SDL_Surface * flipSurface(SDL_Surface * surface)
{
    int current_line,pitch;
    SDL_Surface * fliped_surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
                                   surface->w,surface->h,
                                   surface->format->BitsPerPixel,
                                   surface->format->Rmask,
                                   surface->format->Gmask,
                                   surface->format->Bmask,
                                   surface->format->Amask);

    SDL_LockSurface(surface);
    SDL_LockSurface(fliped_surface);

    pitch = surface->pitch;
    for (current_line = 0; current_line < surface->h; current_line ++)
    {
        memcpy(&((unsigned char* )fliped_surface->pixels)[current_line*pitch],
               &((unsigned char* )surface->pixels)[(surface->h - 1  -
                                                    current_line)*pitch],
               pitch);
    }

    SDL_UnlockSurface(fliped_surface);
    SDL_UnlockSurface(surface);
    return fliped_surface;
}
