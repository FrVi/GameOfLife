#include "Barres.h"
#include "bin_operator.h"

void Projection(float val, float x, float y)
{
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluOrtho2D(x, (screensize+texture_menu+hauteur_barre)/val+x, (screensize+hauteur_barre)/val+y, y); //pour le zoom, mais on doit redessiner alors
	glPointSize(val);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();
}

//MODIFIER CA: on blitte le menu au debut une fois, par la suite on fait des copies d'ecran
//d'une frame � l'autre
void Affichage(GLuint texture, float &val, int x1, int y1, int x2, int y2,int x3, int y3, int x4, int y4, float pos_x, float pos_y)
{
glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluOrtho2D(0, screensize+texture_menu+hauteur_barre, screensize+hauteur_barre, 0);

	glColor3ub(255,255,255);
glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();

glEnable(GL_TEXTURE_2D);
glBindTexture(GL_TEXTURE_2D, texture);
		glBegin(GL_QUADS);
			glTexCoord2d(0,1); glVertex2i(x1,y1);
			glTexCoord2d(1,1); glVertex2i(x2,y2);
			glTexCoord2d(1,0); glVertex2i(x3,y3);
			glTexCoord2d(0,0); glVertex2i(x4,y4);
		glEnd();
	glDisable(GL_TEXTURE_2D);

	Projection(val, pos_x, pos_y);
}

//pas de rotation
void Affichage_Droit(GLuint texture, float &val, int x1, int y1, int x2, int y2, float pos_x, float pos_y)
{
	Affichage(texture,val,x1,y1,x2,y1,x2,y2,x1,y2, pos_x, pos_y);
}

void barres::afficher(float &val, float pos_x, float pos_y)
{
	int a=hauteur_barre;//17 normalement
	int d=screensize-hauteur_barre;

	int b1=(d-a)*pos_x/(taille-1)+a;
	int c1=(d-a)*(pos_x+screensize/val-1)/(taille-1)+a;
	int e1=(b1+c1)/2;

	int b2=(d-a)*pos_y/(taille-1)+a;
	int c2=(d-a)*(pos_y+screensize/val-1)/(taille-1)+a;
	int e2=(b2+c2)/2;
	
	Affichage_Droit(texture1,val,x1,y1,x2,y2, pos_x, pos_y);
	//Affichage(texture,val,0,screensize+barre,screensize,screensize+barre ,screensize,screensize,0,screensize);
	Affichage(texture1,val,x3,y3,x3,y2,x2,y2,x2,y3, pos_x, pos_y);

	//VERIFIER SI SERT ENCORE: ON SORT PAS DE L'ecran donc certians doivent etre inutiles
	//les barres simples (TEXTURE ETIREE)
	if(b1>=hauteur_barre && c1<=screensize-hauteur_barre)
	{
		Affichage(texture2,val,c1,screensize,c1,screensize+hauteur_barre ,b1,screensize+hauteur_barre,b1,screensize, pos_x, pos_y);
	}

	if(b2>=hauteur_barre && c2<=screensize-hauteur_barre)
	{
		Affichage_Droit(texture2,val,screensize,b2,screensize+hauteur_barre,c2, pos_x, pos_y);
	}

	//les centres des barres
	if(e1-5>=hauteur_barre && e1+5<=screensize-hauteur_barre-1)
	{
		Affichage(texture3,val,e1+5,screensize,e1+5,screensize+hauteur_barre ,e1-5,screensize+hauteur_barre,e1-5,screensize, pos_x, pos_y);
	}

	if(e2-5>=hauteur_barre && e2+5<=screensize-hauteur_barre-1)
	{
		Affichage_Droit(texture3,val,screensize,e2-5,screensize+hauteur_barre,e2+5, pos_x, pos_y);
	}


	//afficher les 2 bouts

	if(c1-6>=b1+6)//pas de superposition des bouts
	{
		if(c1<=screensize-hauteur_barre-1)
		{
			Affichage(texture4,val,c1,screensize,c1,screensize+hauteur_barre ,c1-6,screensize+hauteur_barre,c1-6,screensize, pos_x, pos_y);
		}

		if(b1>=17)
		{
			Affichage(texture4,val,b1,screensize,b1,screensize+hauteur_barre ,b1+6,screensize+hauteur_barre,b1+6,screensize, pos_x, pos_y);
		}
	}

	if(c2-6>=b2+6)
	{
		if(b2>=17)
		{
			Affichage_Droit(texture4,val,screensize,b2,screensize+hauteur_barre,b2+6, pos_x, pos_y);
		}

		if(c2<=screensize-hauteur_barre-1)
		{
			Affichage_Droit(texture4,val,screensize,c2,screensize+hauteur_barre,c2-6, pos_x, pos_y);
		}
	}

	//afficher les 2 bouts, qui viennent effacer tout eventuellement. (verifier quand m^me qu'ils s'effacent pas l'un l'autre)
}

void Move(float &x, float &y, float dx, float dy, float val)
{
	float x_temp,y_temp;

	x_temp=x+(dx/val);
	y_temp=y+(dy/val);

	/*if( x_temp<0 || x_temp+taille/val>taille
	 || y_temp<0 || y_temp+taille/val>taille)//elimine le cas du zoom negatif : tjrs >=
	{
		return;
	}*/

	if(x_temp<0)
	{
		x_temp=0;
	}

	if(y_temp<0)
	{
		y_temp=0.0;
	}

	if(x_temp+screensize/val>taille)
	{
		x_temp=taille-screensize/val;
	}

	if(y_temp+screensize/val>taille)
	{
		y_temp=taille-screensize/val;
	}

	x=x_temp;
	y=y_temp;

	Projection(val,x,y);
}

void barres::gauche(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max)
{
	move_cond=true;
	a=-mvt;
	b=0;
	x_min=x3+1;
	x_max=x3+hauteur_barre-2;
	y_min=y3+1;
	y_max=y2-3;
}

void barres::droite(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max)
{
	move_cond=true;
	a=mvt;
	b=0;
	x_min=x1-(hauteur_barre-2)-1;
	x_max=x1-2;
	y_min=y3+1;
	y_max=y2-3;
}

void barres::haut(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max)
{
	move_cond=true;
	a=0;
	b=-mvt;
	x_min=x1+1;
	x_max=x2-3;
	y_min=y1+1;
	y_max=y1+hauteur_barre-2;
}

void barres::bas(bool &move_cond,int &a,int&b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max)
{
	move_cond=true;
	a=0;
	b=mvt;
	x_min=x1+1;
	x_max=x2-3;
	y_min=y2-(hauteur_barre-2)-1;
	y_max=y3-2;
}

barres::barres(GLuint texture1,GLuint texture2,GLuint texture3,GLuint texture4, unsigned int x1, unsigned int x2, unsigned int x3, unsigned int y1, unsigned int y2, unsigned int y3)
{
	this->x1=x1;
	this->x2=x2;
	this->x3=x3;
	this->y1=y1;
	this->y2=y2;
	this->y3=y3;

	this->texture1=texture1;
	this->texture2=texture2;
	this->texture3=texture3;
	this->texture4=texture4;
}

void barres::fleches(int X,int Y,bool &move_cond,int &a,int &b,unsigned int &x_min,unsigned int &x_max,unsigned int &y_min,unsigned int &y_max, bool &cond_barre_horiz, bool &cond_barre_verti)
{
	if(Y>=screensize+1 && Y<=screensize+(hauteur_barre-1)-2)
	{
		if(X>=1 && X<=hauteur_barre-2)
		{
			gauche(move_cond,a,b,x_min,x_max,y_min,y_max);
		}
		else if(X>=screensize-(hauteur_barre-2)-1 && X<=screensize-2)
		{
			droite(move_cond,a,b,x_min,x_max,y_min,y_max);
		}
		else if(X>hauteur_barre-2 && X<screensize-(hauteur_barre-2)-1)//pas fluide (le reste non plus)
		{
			cond_barre_horiz=true;
		}
		//barres de defilement ou coin inutile en bas
	}
	else if(X>=screensize+1 && X<=screensize+(hauteur_barre-1)-2)
	{
		if(Y>=1 && Y<=hauteur_barre-2)
		{
			haut(move_cond,a,b,x_min,x_max,y_min,y_max);
		}
		else if(Y>=screensize-(hauteur_barre-2)-1 && Y<=screensize-2)
		{
			bas(move_cond,a,b,x_min,x_max,y_min,y_max);
		}
		else if(Y>hauteur_barre-2 && Y<screensize-(hauteur_barre-2)-1)//pas fluide (le reste non plus)
		{
			cond_barre_verti=true;
		}
	}
}

void Afficher_Grille(unsigned char* map, float pos_x, float pos_y,float val)
{
glClear(GL_COLOR_BUFFER_BIT);
unsigned int x,y;

	glBegin(GL_POINTS);
		for(x=pos_x;x<screensize/val+pos_x;x++)
		{
			for(y=pos_y;y<screensize/val+pos_y;y++)
			{
				if(BinVal(map,x,y)==1)
				{
					glColor3ub(255,0,0);
				}
				else
				{
					glColor3ub(0,0,0);
				}
				glVertex2f(x, y);
			}
		}
	glEnd();
}